﻿using awang_otomobil_BE.Model.DTO;
using System.Data.SqlClient;
using System.Data;
using Microsoft.AspNetCore.Mvc;
using otomobil_team02_be.Models;
using Microsoft.AspNetCore.Authorization;

namespace awang_otomobil_BE.Controllers
{
    public class MyClassController : ControllerBase
    {
        private readonly IConfiguration _connection;

        public MyClassController(IConfiguration connection)
        {
            _connection = connection;
        }

        [Authorize]
        [HttpGet]
        [Route("get_myclass")]
        public List<MyClassModel> GetMyClass(string? uId)
        {
            List<MyClassModel> myClassList = new List<MyClassModel>();

            using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                conn.Open();

                string queryCart = "select * from myclass where fk_users = @id";
                SqlCommand cmdCart = new SqlCommand(queryCart, conn);
                cmdCart.Parameters.AddWithValue("@id", uId);
                SqlDataAdapter adapterCart = new SqlDataAdapter(cmdCart);
                DataTable dtCart = new DataTable();
                adapterCart.Fill(dtCart);

                for (int i = 0; i < dtCart.Rows.Count; i++)
                {
                    MyClassModel myClass = new MyClassModel();

                    int myclass_id = Convert.ToInt32(dtCart.Rows[i]["myclass_id"].ToString());
                    int users_id = Convert.ToInt32(dtCart.Rows[i]["fk_users"].ToString());
                    int product_id = Convert.ToInt32(dtCart.Rows[i]["fk_product"].ToString());
                    DateTime date = (DateTime)dtCart.Rows[i]["date"];
                    DateTime dateOnly = date.Date;

                    myClass.myclass_id = myclass_id;
                    myClass.fk_users = users_id;
                    myClass.fk_product = product_id;
                    myClass.date = dateOnly;

                    string getProduct = $"select p.*, c.category_name from product p inner join category c on c.category_id = p.fk_category where p.product_id = {product_id}";
                    SqlCommand cmdProduct = new SqlCommand(getProduct, conn);
                    SqlDataAdapter adapterProduct = new SqlDataAdapter(cmdProduct);
                    DataTable dtProduct = new DataTable();
                    adapterProduct.Fill(dtProduct);

                    ProductModels pModels = new ProductModels();

                    pModels.product_id = Convert.ToInt32(dtProduct.Rows[0]["product_id"].ToString());
                    pModels.product_name = dtProduct.Rows[0]["product_name"].ToString() ?? string.Empty;
                    pModels.product_pic = dtProduct.Rows[0]["product_pic"].ToString() ?? string.Empty;
                    pModels.product_price = Convert.ToInt32(dtProduct.Rows[0]["product_price"].ToString());
                    pModels.product_description = dtProduct.Rows[0]["product_description"].ToString() ?? string.Empty;
                    pModels.fk_category = Convert.ToInt32(dtProduct.Rows[0]["fk_category"].ToString());
                    pModels.category = new CategoryModels() {
                        category_name = dtProduct.Rows[0]["category_name"].ToString()
                    };
                    myClass.products = pModels;
                    myClassList.Add(myClass);
                }

                conn.Close();
            }
                return myClassList;
        }

        [Authorize]
        [HttpPost]
        [Route("add_myclass")]
        public string AddMyClass(MyClassModel inputModel) // jika berupa list pakai foreach kalau tidak langsung Add
        {
            List<MyClassModel> AddPay = new List<MyClassModel>();
            string query = "Insert into myclass values(" + inputModel.fk_users + "," + inputModel.fk_product + ")";

            using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

            }

            return "class added successfully";
        }


        // private List<MyClassModel> LoadMyClassDB()
        // {
        //     List<MyClassModel> outputList = new List<MyClassModel>();
        //     string query = "Select * from myclass";
        //     using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
        //     {
        //         //conn.Open();
        //         SqlCommand cmd = new SqlCommand(query, conn);
        //         SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
        //         DataTable dt = new DataTable();
        //         dataAdapter.Fill(dt);

        //         for (int i = 0; i < dt.Rows.Count; i++)
        //         {
        //             MyClassModel ClassModel = new MyClassModel();
        //             ClassModel.myclass_id = Convert.ToInt32(dt.Rows[i]["myclass_id"].ToString());
        //             ClassModel.fk_users = Convert.ToInt32(dt.Rows[i]["fk_users"].ToString());
        //             ClassModel.fk_product = Convert.ToInt32(dt.Rows[i]["fk_users"].ToString());

        //             outputList.Add(ClassModel);
        //         }

        //         //conn.Close();
        //     }
        //     return outputList;
        // }
    }

}
