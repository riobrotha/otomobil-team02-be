﻿using awang_otomobil_BE.Model.DTO;
using System.Data.SqlClient;
using System.Data;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Authorization;
using otomobil_team02_be.Models;

namespace awang_otomobil_BE.Controllers
{
    public class InvoiceController : ControllerBase
    {
        private readonly IConfiguration _connection;

        public InvoiceController(IConfiguration connection)
        {
            _connection = connection;
        }

        public class LoadInvoiceModel
        {
            public int invoice_id { get; set; }

            public DateTime invoice_date {get; set;}

            public int total_course {get; set;}

            public int total_price {get; set;}
        }

        public class LoadInvoiceDetailModel
        {
            public int fk_order { get; set; }
            public int fk_product { get; set; }

            public DateTime date { get; set; }
            public ProductModels products { get; set; }
        }

        public class LoadInvoiceDetailModelDTO
        {

            public int fk_order {get; set;}  
        }

        [Authorize]
        [HttpGet]
        [Route("get_invoice")]
        public List<LoadInvoiceModel> GetInvoice([FromQuery] string? user_id)
        {
            List<LoadInvoiceModel> invoice = new List<LoadInvoiceModel>();
            using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                conn.Open();

                string query = "SELECT i.invoice_id,i.invoice_date,COUNT(di.detail_invoice_id) AS total_course,SUM(p.product_price) AS total_price FROM invoice i INNER JOIN detail_invoice di ON di.fk_order = i.invoice_id INNER JOIN product p on p.product_id = di.fk_product GROUP BY i.invoice_id, i.invoice_date;";
                if (!string.IsNullOrEmpty(user_id)) {
                    query = "SELECT i.invoice_id,i.invoice_date,COUNT(di.detail_invoice_id) AS total_course,SUM(p.product_price) AS total_price FROM invoice i INNER JOIN detail_invoice di ON di.fk_order = i.invoice_id INNER JOIN product p on p.product_id = di.fk_product WHERE fk_users = @fk_users GROUP BY i.invoice_id, i.invoice_date;";
                }
                

                SqlCommand command = new SqlCommand(query, conn);

                if (!string.IsNullOrEmpty(user_id)) {
                    command.Parameters.AddWithValue("@fk_users", int.Parse(user_id));
                }

                using(SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read()) {
                        LoadInvoiceModel loadInvoiceModel = new LoadInvoiceModel()
                        {
                            invoice_id = int.Parse(reader["invoice_id"].ToString()),
                            invoice_date = (DateTime)reader["invoice_date"],
                            total_course = int.Parse(reader["total_course"].ToString()),
                            total_price = int.Parse(reader["total_price"].ToString()),
                        };

                        invoice.Add(loadInvoiceModel);


                    }

                    reader.Close();
                }

                conn.Close();
            }

            return invoice;
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        [Route("get_all_invoice")]
        public List<LoadInvoiceModel> GetAllInvoice()
        {
            List<LoadInvoiceModel> invoice = new List<LoadInvoiceModel>();
            using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                conn.Open();

                string query = "SELECT i.invoice_id,i.invoice_date,COUNT(di.detail_invoice_id) AS total_course,SUM(p.product_price) AS total_price FROM invoice i INNER JOIN detail_invoice di ON di.fk_order = i.invoice_id INNER JOIN product p on p.product_id = di.fk_product INNER JOIN users u on u.users_id = i.fk_users WHERE users_role = 'user' GROUP BY i.invoice_id, i.invoice_date;";
               
                SqlCommand command = new SqlCommand(query, conn);

                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        LoadInvoiceModel loadInvoiceModel = new LoadInvoiceModel()
                        {
                            invoice_id = int.Parse(reader["invoice_id"].ToString()),
                            invoice_date = (DateTime)reader["invoice_date"],
                            total_course = int.Parse(reader["total_course"].ToString()),
                            total_price = int.Parse(reader["total_price"].ToString()),
                        };

                        invoice.Add(loadInvoiceModel);


                    }

                    reader.Close();
                }

                conn.Close();
            }

            return invoice;
        }

        [Authorize]
        [HttpGet]
        [Route("get_detail_invoice")]
        public List<LoadInvoiceDetailModel> GetDetailInvoice([FromQuery]LoadInvoiceDetailModelDTO request)
        {
            List<LoadInvoiceDetailModel> invoiceDetail = new List<LoadInvoiceDetailModel>();

            using (SqlConnection connection = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                connection.Open();

                string queryCart = "select * from detail_invoice where fk_order = @id";
                SqlCommand cmdCart = new SqlCommand(queryCart, connection);
                cmdCart.Parameters.AddWithValue("@id", request.fk_order);
                SqlDataAdapter adapterCart = new SqlDataAdapter(cmdCart);
                DataTable dtCart = new DataTable();
                adapterCart.Fill(dtCart);

                for (int i = 0; i < dtCart.Rows.Count; i++)
                {
                    LoadInvoiceDetailModel detail = new LoadInvoiceDetailModel();
                    int fk_order = Convert.ToInt32(dtCart.Rows[i]["fk_order"].ToString());
                    int product_id = Convert.ToInt32(dtCart.Rows[i]["fk_product"].ToString());
                    DateTime date = (DateTime)dtCart.Rows[i]["detail_invoice_schedule"];
                    DateTime dateOnly = date.Date;

                    detail.fk_order = fk_order;
                    detail.fk_product = product_id;
                    detail.date = dateOnly;

                    string getProduct = $"select p.*, c.category_name from product p inner join category c on c.category_id = p.fk_category where p.product_id = {product_id}";
                    SqlCommand cmdProduct = new SqlCommand(getProduct, connection);
                    SqlDataAdapter adapterProduct = new SqlDataAdapter(cmdProduct);
                    DataTable dtProduct = new DataTable();
                    adapterProduct.Fill(dtProduct);

                    ProductModels pModels = new ProductModels();

                    pModels.product_id = Convert.ToInt32(dtProduct.Rows[0]["product_id"].ToString());
                    pModels.product_name = dtProduct.Rows[0]["product_name"].ToString() ?? string.Empty;
                    pModels.product_pic = dtProduct.Rows[0]["product_pic"].ToString() ?? string.Empty;
                    pModels.product_price = Convert.ToInt32(dtProduct.Rows[0]["product_price"].ToString());
                    pModels.product_description = dtProduct.Rows[0]["product_description"].ToString() ?? string.Empty;
                    pModels.fk_category = Convert.ToInt32(dtProduct.Rows[0]["fk_category"].ToString());
                    pModels.category = new CategoryModels() {
                        category_name = dtProduct.Rows[0]["category_name"].ToString()
                    };
                    detail.products = pModels;
                    invoiceDetail.Add(detail);
                }

                connection.Close();

                return invoiceDetail;
            }


        }


        // [HttpPost]
        // [Route("add_invoice")]
        // public string AddInvoice(InvoiceModel inputModel) // jika berupa list pakai foreach kalau tidak langsung Add
        // {
        //     List<InvoiceModel> AddPay = new List<InvoiceModel>();
        //     string query = "Insert into invoice values('" + inputModel.invoice_date + "'," + inputModel.fk_users + "," + inputModel.fk_payment + ")";

        //     using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
        //     {
        //         SqlCommand cmd = new SqlCommand(query, conn);
        //         conn.Open();
        //         cmd.ExecuteNonQuery();
        //         conn.Close();

        //     }

        //     return "invoice added successfully";
        // }

        public class NewInvoiceModel
        {
            public DateTime invoice_date { get; set; }
            public int fk_payment { get; set; }
            public int fk_users { get; set; }
            public List<InvoiceDetailModel> invoice_details { get; set; }
        }

        public class InvoiceDetailModel
        {
            public DateTime detail_invoice_schedule { get; set; }
            public decimal detail_invoice_price { get; set; }
            public int? fk_product { get; set; }
            public int? cart_id { get; set; }
        }

        [HttpPost]
        [Route("add_invoice")]
        public IActionResult AddInvoice([FromBody]NewInvoiceModel inputModel) // jika berupa list pakai foreach kalau tidak langsung Add
        {
            //List<InvoiceModel> AddPay = new List<InvoiceModel>();
           // string query = "Insert into invoice values('" + DateTime.Now + "'," + inputModel.fk_users + "," + inputModel.fk_payment + ")";
            string query = "Insert into invoice values(@invoice_date, @fk_payment, @fk_users); SELECT SCOPE_IDENTITY();";

            using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                
                using(SqlTransaction transaction = conn.BeginTransaction())
                {
                    try {

                        using (SqlCommand cmd = new SqlCommand(query, conn))
                        {
                            DateTime now = DateTime.Now;

                            DateTime toDate = now.Date;
                            cmd.Parameters.AddWithValue("@invoice_date", toDate);
                            cmd.Parameters.AddWithValue("@fk_payment", inputModel.fk_payment);
                            cmd.Parameters.AddWithValue("@fk_users", inputModel.fk_users);

                            cmd.Transaction = transaction;

                            int invoiceId = Convert.ToInt32(cmd.ExecuteScalar());

                            foreach(var item in inputModel.invoice_details)
                            {
                                string queryInvoiceDetail = "insert into detail_invoice values(@detail_invoice_schedule, @detail_invoice_price, @fk_order, @fk_product)";

                                string queryDeleteCart = "delete from cart WHERE cart_id = @cart_id";

                                string queryMyClass = "insert into myclass values(@fk_users,@fk_product,@date)";

                                using (SqlCommand cmdInvoiceDetail = new SqlCommand(queryInvoiceDetail, conn))
                                {
                                    cmdInvoiceDetail.Parameters.AddWithValue("@detail_invoice_schedule", item.detail_invoice_schedule);
                                    cmdInvoiceDetail.Parameters.AddWithValue("@detail_invoice_price", item.detail_invoice_price);
                                    cmdInvoiceDetail.Parameters.AddWithValue("@fk_order", invoiceId);
                                    cmdInvoiceDetail.Parameters.AddWithValue("@fk_product", item.fk_product);

                                    cmdInvoiceDetail.Transaction = transaction;
                                    cmdInvoiceDetail.ExecuteNonQuery();
                                }
                                
                                using (SqlCommand cmdMyClass = new SqlCommand(queryMyClass, conn))
                                {
                                    cmdMyClass.Parameters.AddWithValue("@fk_users", inputModel.fk_users);
                                    cmdMyClass.Parameters.AddWithValue("@fk_product", item.fk_product);
                                    cmdMyClass.Parameters.AddWithValue("@date", item.detail_invoice_schedule);

                                    cmdMyClass.Transaction = transaction;
                                    cmdMyClass.ExecuteNonQuery();
                                    
                                }


                                using (SqlCommand cmdDeleteCart = new SqlCommand(queryDeleteCart, conn))
                                {
                                    cmdDeleteCart.Parameters.AddWithValue("@cart_id", item.cart_id);
                                    cmdDeleteCart.Transaction = transaction;
                                    cmdDeleteCart.ExecuteNonQuery();
                                }
                            }

                            transaction.Commit();

                            return new JsonResult(new {message = "Add Invoice Successfully"}) {StatusCode = 200};

                        }
                    } catch (Exception ex) {

                        transaction.Rollback();

                        return new JsonResult(new {message = "Error: " + ex.Message}) {StatusCode = 500};
                    }
                }

                
            }
        }


        [HttpPost]
        [Route("add_detail_invoice")]
        public string AddDetailInvoice(DetailInvoiceModel inputModel) // jika berupa list pakai foreach kalau tidak langsung Add
        {
            List<DetailInvoiceModel> AddPay = new List<DetailInvoiceModel>();
            string query = "Insert into detail_invoice values('" + inputModel.detail_invoice_name + "'," + inputModel.detail_invoice_price + "," + inputModel.fk_invoice + "," + inputModel.fk_product + ")";

            using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                SqlCommand cmd = new SqlCommand(query, conn);
                conn.Open();
                cmd.ExecuteNonQuery();
                conn.Close();

            }

            return "detail invoice added successfully";
        }


        // private List<InvoiceModel> LoadInvoiceDB()
        // {
        //     List<InvoiceModel> outputList = new List<InvoiceModel>();
        //     string query = "Select * from invoice";
        //     using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
        //     {
        //         //conn.Open();
        //         SqlCommand cmd = new SqlCommand(query, conn);
        //         SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
        //         DataTable dt = new DataTable();
        //         dataAdapter.Fill(dt);

        //         for (int i = 0; i < dt.Rows.Count; i++)
        //         {
        //             InvoiceModel InvoiceModel = new InvoiceModel();
        //             InvoiceModel.invoice_id = Convert.ToInt32(dt.Rows[i]["invoice_id"].ToString());
        //             InvoiceModel.invoice_date = dt.Rows[i]["invoice_date"].ToString();
        //             InvoiceModel.fk_users = Convert.ToInt32(dt.Rows[i]["fk_users"].ToString());
        //             InvoiceModel.fk_payment = Convert.ToInt32(dt.Rows[i]["fk_payment"].ToString());

        //             outputList.Add(InvoiceModel);
        //         }

        //         //conn.Close();
        //     }
        //     return outputList;
        // }

        
        // private List<DetailInvoiceModel> LoadDetailInvoiceDB()
        // {
        //     List<DetailInvoiceModel> outputList = new List<DetailInvoiceModel>();
        //     string query = "Select * from detail_invoice";
        //     using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
        //     {
        //         //conn.Open();
        //         SqlCommand cmd = new SqlCommand(query, conn);
        //         SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
        //         DataTable dt = new DataTable();
        //         dataAdapter.Fill(dt);

        //         for (int i = 0; i < dt.Rows.Count; i++)
        //         {
        //             DetailInvoiceModel DeInvoiceModel = new DetailInvoiceModel();
        //             DeInvoiceModel.detail_invoice_id = Convert.ToInt32(dt.Rows[i]["detail_invoice_id"].ToString());
        //             DeInvoiceModel.detail_invoice_name = dt.Rows[i]["detail_invoice_name"].ToString();
        //             DeInvoiceModel.detail_invoice_price = Convert.ToInt32(dt.Rows[i]["detail_invoice_price"].ToString());
        //             DeInvoiceModel.fk_invoice = Convert.ToInt32(dt.Rows[i]["fk_invoice"].ToString());
        //             DeInvoiceModel.fk_product = Convert.ToInt32(dt.Rows[i]["fk_product"].ToString());

        //             outputList.Add(DeInvoiceModel);
        //         }

        //         //conn.Close();
        //     }
        //     return outputList;
        // }
    }
}
