using System.Data.SqlClient;
using System.Security.Claims;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using otomobil_team02_be.Models;
using otomobil_team02_be.Models.DTO;

namespace otomobil_team02_be.Controllers;

[ApiController]
[Route("api")]
public class UserController : ControllerBase
{
  private readonly IConfiguration? _config;

  public UserController(IConfiguration config)
  {
    _config = config;
  }

  [Authorize]
  [HttpGet]
  [Route("user")]
  public IActionResult GetUser()
  {
    var claims = HttpContext.User.Claims;
    UserModel? user = null;

    // Mengambil nilai dari claim "emailaddress" (misalnya, alamat email pengguna)
    string userEmail = claims.FirstOrDefault(c => c.Type == ClaimTypes.Email)?.Value ?? string.Empty;


    if (string.IsNullOrEmpty(userEmail)) {
      return new JsonResult(new {message = "User Email claims not Found"}) {StatusCode = 400};
    }

    using (SqlConnection connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
    {
      connection.Open();

      string query = "SELECT TOP 1 * FROM users WHERE users_email = @users_email";

      SqlCommand command = new SqlCommand(query, connection);
      command.Parameters.AddWithValue("@users_email", userEmail.ToLower());

      using(SqlDataReader reader = command.ExecuteReader())
      {
        if (reader.Read())
        {
          user = new UserModel()
          {
            users_id = int.Parse(reader["users_id"].ToString() ?? "0"),
            users_name = reader["users_name"].ToString(),
            users_email = reader["users_email"].ToString(),
            users_address = reader["users_address"].ToString(),
            users_role = reader["users_role"].ToString(),
          };
        }
      }

      connection.Close();

    }

    if (user != null)
    {
      return new JsonResult(new {message = "Get User Profile is successfully", user}) {StatusCode = 200};
    }

    return new JsonResult(new {message = "User Not Found"}) {StatusCode = 400};
    
  }

  [Authorize(Roles = "admin")]
  [HttpGet]
  [Route("users")]
  public IActionResult GetAllUser()
  {
    List<UserModel> users = new List<UserModel>();
    using (SqlConnection connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
    {
      string query = "select * from users";
      connection.Open();

      SqlCommand command = new SqlCommand(query, connection);
      using (SqlDataReader reader = command.ExecuteReader())
      {
        while(reader.Read()) {
          users.Add(new UserModel() {
            users_id = int.Parse(reader["users_id"].ToString() ?? "0"),
            users_name = reader["users_name"].ToString() ?? string.Empty,
            users_email = reader["users_email"].ToString() ?? string.Empty,
            users_address = reader["users_address"].ToString() ?? string.Empty,
            users_role = reader["users_role"].ToString() ?? string.Empty,
            users_is_active = (bool) reader["users_is_active"],
          });
        }

        reader.Close();
      }

      connection.Close();
    }

    return Ok(users);

  }

  [Authorize(Roles = "admin")]
  [HttpPost]
  [Route("user")]
  public IActionResult AddUser([FromBody] UserModel request)
  {
    string tempPassword = BCrypt.Net.BCrypt.HashPassword("12345678");
    using (SqlConnection connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
    {
      string insertQuery = "insert into users (users_name, users_email, users_password, users_role) values(@users_name, @users_email, @users_password, @users_role)";

      try {
        connection.Open();

        SqlCommand command = new SqlCommand(insertQuery, connection);

        command.Parameters.AddWithValue("@users_name", request.users_name);
        command.Parameters.AddWithValue("@users_email", request.users_email);
        command.Parameters.AddWithValue("@users_password", tempPassword);
        command.Parameters.AddWithValue("@users_role", request.users_role.ToLower());

        int rowsAffected = command.ExecuteNonQuery();

        if (rowsAffected > 0) {
          return new JsonResult(new {message = "Add User Successfully"}) {StatusCode = 200};
        }
      } catch(Exception ex) {
        return new JsonResult(new {message = "Error: " + ex.Message}) {StatusCode = 500};
      } finally {
        connection.Close();
      }

      
    }

    return BadRequest();
  }

  [Authorize(Roles = "admin")]
  [HttpPut]
  [Route("activate_user")]
  public IActionResult ActivateUser([FromBody] ActivateUserDTO request)
  {
    using (SqlConnection connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
    {
      try {
        connection.Open();

        string query = "UPDATE users SET users_is_active = @users_is_active WHERE users_id = @users_id;";
        SqlCommand command = new SqlCommand(query, connection);
        command.Parameters.AddWithValue("@users_id", request.users_id);
        command.Parameters.AddWithValue("@users_is_active", request.users_is_active);

        command.ExecuteNonQuery();
        return new JsonResult(new {message = "Update User Successfully"}) {StatusCode = 200};

      } catch (Exception ex) {
        object response = new {message = "Error: " + ex.Message};
        return new JsonResult(response) {StatusCode = 500};
      } finally {
        connection.Close();
      }
    }
  }

  [Authorize(Roles = "admin")]
  [HttpPut]
  [Route("update_user")]
  public IActionResult UpdateUser([FromBody] UserModel request)
  {
    using (SqlConnection connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
    {
      try {
        connection.Open();

        string query = "UPDATE users SET users_name = @users_name, users_email = @users_email, users_role = @users_role WHERE users_id = @users_id;";
        SqlCommand command = new SqlCommand(query, connection);
        command.Parameters.AddWithValue("@users_id", request.users_id);
        command.Parameters.AddWithValue("@users_name", request.users_name);
        command.Parameters.AddWithValue("@users_email", request.users_email);
        command.Parameters.AddWithValue("@users_role", request.users_role.ToLower());

        command.ExecuteNonQuery();
        return new JsonResult(new {message = "Update User Successfully"}) {StatusCode = 200};

      } catch (Exception ex) {
        object response = new {message = "Error: " + ex.Message};
        return new JsonResult(response) {StatusCode = 500};
      } finally {
        connection.Close();
      }
    }
  }
}
