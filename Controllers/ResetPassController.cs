using System.Data.SqlClient;
using System.Net;
using System.Web;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using otomobil_team02_be.Helpers;
using otomobil_team02_be.Models.DTO;
using otomobil_team02_be.Services.EmailService;

namespace otomobil_team02_be.Controllers;

[ApiController]
[Route("api")]
public class ResetPassController : ControllerBase
{
  private readonly IConfiguration? _config;
  private readonly IEmailService? _emailService;

  public ResetPassController(IConfiguration config, IEmailService emailService)
  {
    _config = config;
    _emailService = emailService;
  }

  [AllowAnonymous]
  [HttpPost]
  [Route("validate_email")]
  public IActionResult EmailValidation([FromBody] EmailValidationDTO request)
  {

    string email = string.Empty;
    string users_id = string.Empty;

    using(SqlConnection connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
    {
      connection.Open();

      string query = "SELECT TOP 1 users_id, users_email FROM users WHERE users_email = @users_email AND users_is_active = 1";

      SqlCommand command = new SqlCommand(query, connection);
      command.Parameters.AddWithValue("@users_email", request.email.ToLower());

      using (SqlDataReader reader = command.ExecuteReader())
      {
        if (reader.Read())
        {
          email = reader["users_email"].ToString() ?? string.Empty;
          users_id = EncryptionHelper.Encrypt(reader["users_id"].ToString() ?? string.Empty);
        }
      }
    }

    if (!string.IsNullOrEmpty(email))
    {
      return new JsonResult(new {message = "Email was verified", user_id = users_id}) {StatusCode = 200};
    }

    return new JsonResult(new {message = "Email not found"}) {StatusCode = 400};
  }

  [AllowAnonymous]
  [HttpPut]
  [Route("new_password")]
  public IActionResult CreateNewPassword([FromBody] CreateNewPassDTO request)
  {
    string hashedPassword = BCrypt.Net.BCrypt.HashPassword(request.users_password.ToString());
    int user_id = int.Parse(EncryptionHelper.Decrypt(request.users_id.ToString()));
    
    
    using (SqlConnection connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
    {
      connection.Open();

      string query = "SELECT TOP 1 * FROM users WHERE users_id = @users_id";
      SqlCommand selectCmd = new SqlCommand(query, connection);
      selectCmd.Parameters.AddWithValue("@users_id", user_id);

      using (SqlDataReader reader = selectCmd.ExecuteReader())
      {
        if (reader.Read())
        {
          try {
            reader.Close();
            string queryUpdate = "UPDATE users SET users_password = @users_password WHERE users_id = @users_id";
            SqlCommand updateCmd = new SqlCommand(queryUpdate, connection);
            updateCmd.Parameters.AddWithValue("@users_password", hashedPassword);
            updateCmd.Parameters.AddWithValue("@users_id", user_id);

            updateCmd.ExecuteNonQuery();
            
          } catch (Exception ex) {
            return new JsonResult(new {message = "Error: " + ex.Message}) {StatusCode = 500};
          } finally {
            connection.Close();
          }
        } else {
          return new JsonResult(new {message = "User Not Found"}) {StatusCode = 400};
        }
      }

      connection.Close();

      return new JsonResult(new {message = "Update Password successfully"}) {StatusCode = 200};
    }
  }

  [AllowAnonymous]
  [HttpPost]
  [Route("reset-password/create-form")]
  public IActionResult CreateForm(CreateResetPassFormDTO request)
  {

    int user_id = int.Parse(EncryptionHelper.Decrypt(request.users_id.ToString()));

    using (SqlConnection connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
    {
      connection.Open();

      try {
        string selectQuery = "SELECT TOP 1 * FROM users WHERE users_id = @users_id AND users_is_active = 1";
        SqlCommand selectCmd = new SqlCommand(selectQuery, connection);
        selectCmd.Parameters.AddWithValue("@users_id", user_id);
        
        string users_name = string.Empty;
        string users_email = string.Empty;

        using (SqlDataReader reader = selectCmd.ExecuteReader())
        {
          if (reader.Read())
          {
            users_name = reader["users_name"].ToString();
            users_email = reader["users_email"].ToString().ToLower();
          } else
          {
            return new JsonResult(new {message = "Users not found"}) {StatusCode = 400};
          }

          reader.Close();
        }


        string query = "INSERT INTO reset_password_req (fk_users, expired_time) VALUES(@users_id, @expired_time); SELECT SCOPE_IDENTITY()";
        SqlCommand command = new SqlCommand(query, connection);
        command.Parameters.AddWithValue("@users_id", user_id);
        command.Parameters.AddWithValue("@expired_time",DateTime.Now.AddMinutes(10));
        int lastInsertedReqID = Convert.ToInt32(command.ExecuteScalar());

        string lastInsertedIDConv = EncryptionHelper.Encrypt(lastInsertedReqID.ToString());

        string encodeLastInsertedID = WebUtility.UrlEncode(lastInsertedIDConv);
        string encodeUsersID = WebUtility.UrlEncode(request.users_id);

        EmailDTO emailDTO = new EmailDTO()
        {
          To = users_email,
          Subject = "Reset Password Confirmation",
          Body = $"<h4>Hello {users_name}</h4><p>This is your create password form with click this link <a href='http://52.237.194.35:2026/api/reset-password/verify?q={encodeUsersID}&rid={encodeLastInsertedID}'>http://52.237.194.35:2026/api/reset-password/verify?q={request.users_id}&rid={lastInsertedIDConv}</a></p>"
        };

        _emailService.SendEmail(emailDTO);
      } catch (Exception ex) {
        return new JsonResult(new {message = "Error: " + ex.Message}) {StatusCode = 500};
      } finally {
        connection.Close();
      }

      connection.Close();

      return new JsonResult(new {message = "Create Form Successfully"}) {StatusCode = 200};
    }
    
    
  }

  [AllowAnonymous]
  [HttpGet]
  [Route("reset-password/verify")]
  public IActionResult VerifyForm([FromQuery] string q, string rid)
  {
    string decodeHtmlQ = WebUtility.HtmlDecode(q);
    int user_id = int.Parse(EncryptionHelper.Decrypt(decodeHtmlQ));

    string decideHtmlRid = WebUtility.HtmlDecode(rid);
    int reset_password_req_id = int.Parse(EncryptionHelper.Decrypt(decideHtmlRid));

    using (SqlConnection connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
    {
      try {
        connection.Open();

        string query = "SELECT TOP 1 * FROM reset_password_req WHERE fk_users = @users_id AND reset_password_req_id = @rid ORDER BY reset_password_req_id DESC";
        SqlCommand command = new SqlCommand(query, connection);
        command.Parameters.AddWithValue("@users_id", user_id);
        command.Parameters.AddWithValue("@rid", reset_password_req_id);

        using (SqlDataReader reader = command.ExecuteReader())
        {
          if (reader.Read())
          {
            if (DateTime.Now <= (DateTime)reader["expired_time"])
            {
              reader.Close();
              string queryUpdate = "UPDATE reset_password_req SET expired_time = @expired_time WHERE reset_password_req_id = @rid";
              SqlCommand updateCmd = new SqlCommand(queryUpdate, connection);
              DateTime now = DateTime.Now;
              updateCmd.Parameters.AddWithValue("@expired_time", now.Add(TimeSpan.FromMinutes(-10))); //mengurangi datime sekarang ke 10 menit
              updateCmd.Parameters.AddWithValue("@rid", reset_password_req_id);

              updateCmd.ExecuteNonQuery();
    

              return Redirect(_config.GetSection("ClientUrl").Value + "/reset-password?type=create&q=" + WebUtility.UrlEncode(q));

              //return new JsonResult(new {message = "Form is valid"}) {StatusCode = 200};
            } else 
            {
              return new JsonResult(new {message = "Form is not valid"}) {StatusCode = 403};
            }
          }

          
        }

        
      } catch(Exception ex) {
        return new JsonResult(new {message = "Error: " + ex.Message}) {StatusCode = 500};
      }
    }

    return new JsonResult(new {message = "Request Reset Password Form not Found"}) {StatusCode = 400};
  }
}
