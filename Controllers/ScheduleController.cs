using System.Data.SqlClient;
using Microsoft.AspNetCore.Mvc;
using otomobil_team02_be.Models;


namespace otomobil_team02_be.Controllers;

[ApiController]
[Route("api")]
public class ScheduleController : ControllerBase
{

  private readonly IConfiguration? _config;

  public ScheduleController(IConfiguration config)
  {
    _config = config;
  }

  [HttpGet]
  [Route("schedule")]
  public IActionResult GetSchedule([FromQuery] int product_id)
  {
    List<ScheduleModel> schedules = new List<ScheduleModel>();
    using (SqlConnection connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
    {
      connection.Open();

      string query = "select * from schedule where fk_product = @product_id";
      SqlCommand command = new SqlCommand(query, connection);
      command.Parameters.AddWithValue("@product_id", product_id);

      using(SqlDataReader reader = command.ExecuteReader())
      {
        while (reader.Read())
        {
          schedules.Add(new ScheduleModel() {
            date = (DateTime) reader["date"]
          });
        }

        reader.Close();
      }

      connection.Close();

    }

    return Ok(schedules);
  }
}
