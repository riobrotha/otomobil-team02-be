using System.Data.SqlClient;
using System.Net;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using otomobil_team02_be.Helpers;
using otomobil_team02_be.Models;
using otomobil_team02_be.Models.DTO;
using otomobil_team02_be.Services.EmailService;

namespace otomobil_team02_be.Controllers;

[ApiController]
[Route("api")]
public class AuthController : ControllerBase
{
  private readonly IConfiguration? _config;
  private readonly IEmailService? _emailService;

  public AuthController(IConfiguration config, IEmailService emailService)
  {
    _config = config;
    _emailService = emailService;
  }

  [Authorize]
  [HttpGet]
  [Route("tes")]
  public IActionResult Tes()
  {
    return Ok("mantap");
  }

  [AllowAnonymous]
  [HttpPost]
  [Route("login")]
  public IActionResult Login([FromBody] LoginDTO request)
  {
    var user = Authenticated(request);

    if (user != null)
    {
      var token = GenerateTokenHelper.CreateToken(user); 
      return new JsonResult(new {message = "Login Successfully",access_token = token, user}) {StatusCode = 200};
    }

    return new JsonResult(new {message = "Login Failed"}) {StatusCode = 404};
  }

  private UserModel Authenticated(LoginDTO request)
  {
    UserModel? user = null;

    using(SqlConnection connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
    {
        connection.Open();

        string query = "SELECT TOP 1 * FROM users WHERE users_email = @users_email AND users_is_active = 1";

        SqlCommand command = new SqlCommand(query, connection);
        command.Parameters.AddWithValue("@users_email", request.email.ToLower());

        using (SqlDataReader reader = command.ExecuteReader())
        {
          if (reader.Read())
          {
            string? hashedPasswordFromDb = reader["users_password"].ToString();

            if (BCrypt.Net.BCrypt.Verify(request.password, hashedPasswordFromDb))
            {
              user = new UserModel()
              {
                users_id = int.Parse(reader["users_id"].ToString() ?? "0"),
                users_name = reader["users_name"].ToString(),
                users_email = reader["users_email"].ToString(),
                users_address = reader["users_address"].ToString(),
                users_role = reader["users_role"].ToString(),
              };
            }
          }
        }

        connection.Close();
      
    }

    return user;
  }

  [AllowAnonymous]
  [HttpPost]
  [Route("register")]
  public IActionResult Register([FromBody]RegisterDTO request)
  {
    if (!ModelState.IsValid) {
      return BadRequest(ModelState);
    }


    using (SqlConnection connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
    {
      int lastInsertedID = 0;
      try{
        connection.Open();
        
        if (!IsEmailUnique(request.users_email, connection))
        {
          ModelState.AddModelError("users_email", "email address already exists.");
          return BadRequest(ModelState);
        }

        string query = "INSERT INTO users (users_name, users_email, users_password, users_address) VALUES(@users_name, @users_email, @users_password, @users_address); SELECT SCOPE_IDENTITY();";

        SqlCommand command = new SqlCommand(query, connection);
        command.Parameters.AddWithValue("@users_name", request.users_name);
        command.Parameters.AddWithValue("@users_email", request.users_email.ToLower());
        command.Parameters.AddWithValue("@users_password", BCrypt.Net.BCrypt.HashPassword(request.users_password));
        command.Parameters.AddWithValue("@users_address", request.users_address ?? DBNull.Value.ToString());

        lastInsertedID = Convert.ToInt32(command.ExecuteScalar());

        string lastInsertedIDConv = EncryptionHelper.Encrypt(lastInsertedID.ToString());

        string encodeLastInsertedID = WebUtility.UrlEncode(lastInsertedIDConv);

        EmailDTO emailDTO = new()
        {
            Subject = "Verify Your Email Address",
            To = request.users_email,
            Body = $"<h4>Hello {request.users_name}</h4><p>Thankyou for register</p><p>Please confirm your email with click this link <a href='http://52.237.194.35:2026/api/confirm_email?q={encodeLastInsertedID}'>http://52.237.194.35:2026/api/confirm_email?q={lastInsertedIDConv}</a></p>"
        };

        try {
          _emailService.SendEmail(emailDTO);
        } catch (Exception ex) {
          string queryDel = "DELETE FROM users WHERE users_id = @users_id";

          using (SqlCommand cmd = new SqlCommand(queryDel, connection)) 
          {
            cmd.Parameters.AddWithValue("@users_id", lastInsertedID);
            cmd.ExecuteNonQuery();
          }
          return new JsonResult(new {message = "Error: " + ex.Message}) {StatusCode = 500};
        }

        object response = new {message = "Register User is successfully, please check your email" };
        return new JsonResult(response) {StatusCode = 200};
        
      }catch(Exception ex) {
        return new JsonResult(new {message = "Error: " + ex.Message}) {StatusCode = 500};
      }finally {
        connection.Close();
      }

      
        

        
      
    }
  }

  [AllowAnonymous]
  [HttpGet]
  [Route("confirm_email")]
  public IActionResult ActivateUser([FromQuery] string q)
  {
    string decodeHtmlQ = WebUtility.HtmlDecode(q);
    int user_id = int.Parse(EncryptionHelper.Decrypt(decodeHtmlQ).ToString());

    using (SqlConnection connection = new SqlConnection(_config.GetConnectionString("DefaultConnection")))
    {
      try {
        connection.Open();

        string query = "UPDATE users SET users_is_active = 1 WHERE users_id = @users_id;";
        SqlCommand command = new SqlCommand(query, connection);
        command.Parameters.AddWithValue("@users_id", user_id);

        command.ExecuteNonQuery();
        return Redirect(_config.GetSection("ClientUrl").Value + "/register/success");

      } catch (Exception ex) {
        object response = new {message = "Error: " + ex.Message};
        return new JsonResult(response) {StatusCode = 500};
      } finally {
        connection.Close();
      }
    }
    
  }


  private bool IsEmailUnique(string email, SqlConnection connection)
  {
    string checkEmailUnique = "SELECT COUNT(*) FROM users WHERE users_email = @users_email";
    SqlCommand checkEmailUniqueCommand = new SqlCommand(checkEmailUnique, connection);
    checkEmailUniqueCommand.Parameters.AddWithValue("@users_email", email);
    int emailCount = (int)checkEmailUniqueCommand.ExecuteScalar();

    return emailCount == 0;
  }
}
