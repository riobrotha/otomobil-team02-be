﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using otomobil_team02_be.Models;
using System.Data;
using System.Data.SqlClient;
using System.Security.Cryptography;

namespace otomobil_team02_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        public IConfiguration _configuration;

        public ProductController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [HttpGet]
        [Route("get_products")]
        public List<ProductModels> GetProducts(int limit, int category_id)
        {
            List<ProductModels> products = new List<ProductModels>();

            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                string querryUser = "select * from product p inner join category c on c.category_id = p.fk_category";
                SqlCommand cmd = new SqlCommand(querryUser, conn);
                if (category_id != 0)
                {
                    querryUser = "select * from product p inner join category c on c.category_id = p.fk_category where p.fk_category=@category";
                    cmd = new SqlCommand(querryUser, conn);
                    cmd.Parameters.AddWithValue("@category", category_id);
                }
                SqlDataAdapter sqlDataAdapter = new SqlDataAdapter(cmd);
                DataTable dataTable = new DataTable();
                sqlDataAdapter.Fill(dataTable);

                if (limit > 0)
                {
                    for (int i = 0; i < limit; i++)
                    {
                        ProductModels pModels = new ProductModels();
                        pModels.product_id = Convert.ToInt32(dataTable.Rows[i]["product_id"].ToString());
                        pModels.product_name = dataTable.Rows[i]["product_name"].ToString() ?? string.Empty;
                        pModels.product_pic = dataTable.Rows[i]["product_pic"].ToString() ?? string.Empty;
                        pModels.product_price = Convert.ToInt32(dataTable.Rows[i]["product_price"].ToString());
                        pModels.product_description = dataTable.Rows[i]["product_description"].ToString() ?? string.Empty;
                        pModels.fk_category = Convert.ToInt32(dataTable.Rows[i]["fk_category"].ToString());
                        products.Add(pModels);

                    }
                }
                else
                {
                    for (int i = 0; i < dataTable.Rows.Count; i++)
                    {
                        ProductModels pModels = new ProductModels();
                        pModels.product_id = Convert.ToInt32(dataTable.Rows[i]["product_id"].ToString());
                        pModels.product_name = dataTable.Rows[i]["product_name"].ToString() ?? string.Empty;
                        pModels.product_pic = dataTable.Rows[i]["product_pic"].ToString() ?? string.Empty;
                        pModels.product_price = Convert.ToInt32(dataTable.Rows[i]["product_price"].ToString());
                        pModels.product_description = dataTable.Rows[i]["product_description"].ToString() ?? string.Empty;
                        pModels.fk_category = Convert.ToInt32(dataTable.Rows[i]["fk_category"].ToString());
                        pModels.category = new CategoryModels() {
                            category_id = Convert.ToInt32(dataTable.Rows[i]["category_id"].ToString()),
                            category_name = dataTable.Rows[i]["category_name"].ToString() ?? string.Empty,
                            category_description = dataTable.Rows[i]["category_description"].ToString() ?? string.Empty,
                            category_pic = dataTable.Rows[i]["category_pic"].ToString() ?? string.Empty
                        };
                        products.Add(pModels);

                    }
                }
            }

            return products;
        }

        [HttpGet]
        [Route("get_detail_product")]
        public ProductModels? GetDetailProduct(int id)
        {
            ProductModels prodItem = null;

            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                string querryUser = "select * from product p inner join category c on c.category_id = p.fk_category where p.product_id=@id";
                SqlCommand cmd = new SqlCommand(querryUser, conn);
                cmd.Parameters.AddWithValue("@id", id);
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    prodItem = new ProductModels
                    {
                        product_id = Convert.ToInt32(reader["product_id"].ToString()),
                        product_name = reader["product_name"].ToString() ?? string.Empty,
                        product_pic = reader["product_pic"].ToString() ?? string.Empty,
                        product_price = Convert.ToInt32(reader["product_price"].ToString()),
                        product_description = reader["product_description"].ToString() ?? string.Empty,
                        fk_category = Convert.ToInt32(reader["fk_category"].ToString()),
                        category = new CategoryModels()
                        {
                            category_id = Convert.ToInt32(reader["category_id"].ToString()),
                            category_name = reader["category_name"].ToString() ?? string.Empty,
                            category_description = reader["category_description"].ToString() ?? string.Empty,
                            category_pic = reader["category_pic"].ToString() ?? string.Empty,
                        }
                    };
                }
                reader.Close();
            }

            return prodItem;
        }

        [HttpGet]
        [Route("get_category")]
        public List<CategoryModels>? GetCategory(int? id)
        {
            List<CategoryModels> prodItem = new List<CategoryModels>();

            using (SqlConnection conn = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                conn.Open();
                string querryUser = "select * from category";
                if (id != null) {
                    querryUser = "select * from category where category_id=@id";
                }
                SqlCommand cmd = new SqlCommand(querryUser, conn);

                if (id != null) {
                    cmd.Parameters.AddWithValue("@id", id);
                }
                SqlDataReader reader = cmd.ExecuteReader();

                while (reader.Read())
                {
                    CategoryModels category = new CategoryModels
                    {
                        category_id = Convert.ToInt32(reader["category_id"].ToString()),
                        category_name = reader["category_name"].ToString(),
                        category_description = reader["category_description"].ToString(),
                        category_pic = reader["category_pic"].ToString()
                    };
                    prodItem.Add(category);
                    
                }
                reader.Close();
            }

            return prodItem;
        }
    }
}
  