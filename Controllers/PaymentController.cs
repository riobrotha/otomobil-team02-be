﻿using System.Data;
using System.Data.SqlClient;
using System.Security.Claims;
using awang_otomobil_BE.Model.DTO;
using otomobil_team02_be.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace awang_otomobil_BE.Controllers
{
    public class PaymentController : ControllerBase
    {
        private readonly IConfiguration _connection;
        private readonly IWebHostEnvironment _webHostEnvironment;

        public PaymentController(IConfiguration connection, IWebHostEnvironment webHostEnvironment)
        {
            _connection = connection;
            _webHostEnvironment = webHostEnvironment;
        }

        [Authorize]
        [HttpGet]
        [Route("payment")]
        public IActionResult GetPayment()
        {
             PaymentModel? payment = null;

            using (SqlConnection connection = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                connection.Open();

                string query = "SELECT TOP 1 * FROM payment WHERE payment_name = @payment_name";

                SqlCommand command = new SqlCommand(query, connection);
                //command.Parameters.AddWithValue("@users_email", userEmail.ToLower());

                using(SqlDataReader reader = command.ExecuteReader())
                {
                    if (reader.Read())
                    {
                        payment = new PaymentModel()
                        {
                            payment_id = int.Parse(reader["payment_id"].ToString() ?? "0"),
                            payment_name = reader["payment_name"].ToString(),
                            payment_number = reader["payment_number"].ToString(),
                            payment_pic = reader["payment_pic"].ToString(),
                        };
                    }
                 }

                connection.Close();

            }

            if (payment != null)
            {
                return new JsonResult(new {message = "Get Payment Bio is successfully", payment}) {StatusCode = 200};
            }

            return new JsonResult(new {message = "Payamnet Not Found"}) {StatusCode = 400};
    
        }

        [Authorize(Roles = "admin")]
        [HttpGet]
        [Route("payments")]
        public IActionResult GetAllUser()
        {
            List<PaymentModel> pays = new List<PaymentModel>();
            using (SqlConnection connection = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                string query = "select * from payment";
                connection.Open();

                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while(reader.Read()) {
                        pays.Add(new PaymentModel() {
                        payment_id = int.Parse(reader["payment_id"].ToString() ?? "0"),
                        payment_name = reader["payment_name"].ToString() ?? string.Empty,
                        payment_number = reader["payment_number"].ToString() ?? string.Empty,
                        payment_pic = reader["payment_pic"].ToString() ?? string.Empty,
                        payment_is_active = (bool) reader["payment_is_active"],
                        });
                    }
                    reader.Close();
                }
                connection.Close();
            }
            return Ok(pays);
        }

        [Authorize]
        [HttpGet]
        [Route("payment_active")]
        public IActionResult GetAllPaymentActive()
        {
            List<PaymentModel> pays = new List<PaymentModel>();
            using (SqlConnection connection = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                string query = "select * from payment where payment_is_active = 1";
                connection.Open();

                SqlCommand command = new SqlCommand(query, connection);
                using (SqlDataReader reader = command.ExecuteReader())
                {
                    while(reader.Read()) {
                        pays.Add(new PaymentModel() {
                        payment_id = int.Parse(reader["payment_id"].ToString() ?? "0"),
                        payment_name = reader["payment_name"].ToString() ?? string.Empty,
                        payment_number = reader["payment_number"].ToString() ?? string.Empty,
                        payment_pic = reader["payment_pic"].ToString() ?? string.Empty,
                        payment_is_active = (bool) reader["payment_is_active"],
                        });
                    }
                    reader.Close();
                }
                connection.Close();
            }
            return Ok(pays);
        }

        public class AddPaymentModel
        {
            public string? payment_name { get; set; }
            public string? payment_number { get; set; }
            public IFormFile? payment_pic { get; set; }
        }

        [Authorize(Roles = "admin")]
        [HttpPost]
        [Route("add_payment")]
        public IActionResult AddPayment([FromForm] AddPaymentModel request)
        {
            try {
                if (request.payment_pic == null || request.payment_pic.Length <= 0) {
                    return new JsonResult(new {message = "Image file is missing"}) {StatusCode = 400};
                }

                string imageFileName = $"{Guid.NewGuid()}_{ReplaceInvalidChars(request.payment_pic.FileName)}";
                string imagePath = Path.Combine(_webHostEnvironment.ContentRootPath, "wwwroot","Images", imageFileName);

                using (var fileStream = new FileStream(imagePath, FileMode.Create))
                {
                    request.payment_pic.CopyTo(fileStream);
                }

                var paymentImageModel = new PaymentModel
                {
                    payment_name = request.payment_name,
                    payment_number = request.payment_number,
                    payment_pic = "images/" + imageFileName
                };

                using (SqlConnection connection = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
                {
                    string insertQuery = "insert into payment (payment_name, payment_number, payment_pic) values(@payment_name, @payment_number, @payment_pic)";

                    try {
                        connection.Open();

                        SqlCommand command = new SqlCommand(insertQuery, connection);

                        command.Parameters.Add("@payment_name", SqlDbType.NVarChar).Value = paymentImageModel.payment_name;
                        command.Parameters.Add("@payment_number", SqlDbType.NVarChar).Value = paymentImageModel.payment_number;
                        command.Parameters.Add("@payment_pic", SqlDbType.NVarChar).Value = paymentImageModel.payment_pic;

                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected > 0) {
                            return new JsonResult(new {message = "Add User Successfully", logo = paymentImageModel.payment_pic}) {StatusCode = 200};
                        }
                    } catch(Exception ex) {
                        return new JsonResult(new {message = "Error: " + ex.Message}) {StatusCode = 500};
                    } finally {
                        connection.Close();
                    }
                }


            } catch(Exception ex) {
                return new JsonResult(new {message = "Error: " + ex.Message}) {StatusCode = 500};
            }
            //string tempPassword = BCrypt.Net.BCrypt.HashPassword("12345678");
            
            return BadRequest();
        }

        // Add the ReplaceInvalidChars method here
        private string ReplaceInvalidChars(string fileName)
        {
            char[] invalidChars = Path.GetInvalidFileNameChars();
            char[] invalidCharsWithSpace = invalidChars.Concat(new[] { ' ' }).ToArray(); // Menambahkan spasi sebagai karakter ilegal
            return new string(fileName.Select(c => invalidCharsWithSpace.Contains(c) ? '_' : c).ToArray());
        }


        [Authorize(Roles = "admin")]
        [HttpPut]
        [Route("activate_payment")]
        public IActionResult ActivatePayment([FromBody] PaymentModel request)
        {
            using (SqlConnection connection = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                try {
                    connection.Open();

                    string query = "UPDATE payment SET payment_is_active = @payment_is_active WHERE payment_id = @payment_id;";
                    SqlCommand command = new SqlCommand(query, connection);
                    command.Parameters.AddWithValue("@payment_id", request.payment_id);
                    command.Parameters.AddWithValue("@payment_is_active", request.payment_is_active);

                    command.ExecuteNonQuery();
                    return new JsonResult(new {message = "Update Payment Status Successfully"}) {StatusCode = 200};

                } catch (Exception ex) {
                    object response = new {message = "Error: " + ex.Message};
                    return new JsonResult(response) {StatusCode = 500};
                } finally {
                    connection.Close();
                }
            }
        }     

        [Authorize(Roles = "admin")]
        [HttpPut]
        [Route("update_payment")]
        public IActionResult UpdatePayment([FromBody] PaymentModel request){
            using (SqlConnection connection = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                try {
                connection.Open();

                string query = "UPDATE payment SET payment_name = @payment_name, payment_number = @payment_number WHERE payment_id = @payment_id;";
                SqlCommand command = new SqlCommand(query, connection);
                command.Parameters.AddWithValue("@payment_id", request.payment_id);
                command.Parameters.AddWithValue("@payment_name", request.payment_name);
                command.Parameters.AddWithValue("@payment_number", request.payment_number);

                command.ExecuteNonQuery();
                return new JsonResult(new {message = "Update Payment Successfully"}) {StatusCode = 200};

            } catch (Exception ex) {
                object response = new {message = "Error: " + ex.Message};
                return new JsonResult(response) {StatusCode = 500};
            } finally {
                connection.Close();
            }
            }        
        }



        
        private List<PaymentModel> LoadPaymentDB()
        {
            List<PaymentModel> outputList = new List<PaymentModel>();
            string query = "Select * from payment";
            using (SqlConnection conn = new SqlConnection(_connection.GetConnectionString("DefaultConnection")))
            {
                //conn.Open();
                SqlCommand cmd = new SqlCommand(query, conn);
                SqlDataAdapter dataAdapter = new SqlDataAdapter(cmd);
                DataTable dt = new DataTable();
                dataAdapter.Fill(dt);

                for(int i = 0; i < dt.Rows.Count; i++)
                {
                    PaymentModel PayModel = new PaymentModel();
                    PayModel.payment_id = Convert.ToInt32(dt.Rows[i]["payment_id"].ToString());
                    PayModel.payment_name = dt.Rows[i]["payment_name"].ToString();
                    PayModel.payment_number = dt.Rows[i]["payment_number"].ToString();
                    PayModel.payment_pic = dt.Rows[i]["payment_pic"].ToString();
                    PayModel.payment_is_active = (bool)dt.Rows[i]["payment_is_active"];

                    outputList.Add(PayModel);
                }

                //conn.Close();
            }
            return outputList;
        }
    }
        
}
