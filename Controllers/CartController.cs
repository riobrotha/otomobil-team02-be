﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using otomobil_team02_be.Models;
using System.Data.Common;
using System.Data;
using System.Data.SqlClient;
using Microsoft.AspNetCore.Authorization;
using otomobil_team02_be.Models.DTO;

namespace otomobil_team02_be.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartController : ControllerBase
    {
        public IConfiguration _configuration;

        public CartController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        [Authorize]
        [HttpGet]
        [Route("get_cart")]
        public List<CartModels> GetUsers(string? uId)
        {

            List<CartModels> cart = new List<CartModels>();

            using (SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                connection.Open();

                string queryCart = "select * from cart where fk_users = @id";
                SqlCommand cmdCart = new SqlCommand(queryCart, connection);
                cmdCart.Parameters.AddWithValue("@id", uId);
                SqlDataAdapter adapterCart = new SqlDataAdapter(cmdCart);
                DataTable dtCart = new DataTable();
                adapterCart.Fill(dtCart);

                for (int i = 0; i < dtCart.Rows.Count; i++)
                {
                    CartModels cartModel = new CartModels();

                    int cart_id = Convert.ToInt32(dtCart.Rows[i]["cart_id"].ToString());
                    int users_id = Convert.ToInt32(dtCart.Rows[i]["fk_users"].ToString());
                    int product_id = Convert.ToInt32(dtCart.Rows[i]["fk_product"].ToString());
                    DateTime date = (DateTime)dtCart.Rows[i]["date"];
                    DateTime dateOnly = date.Date;

                    cartModel.cart_id = cart_id;
                    cartModel.fk_users = users_id;
                    cartModel.fk_product = product_id;
                    cartModel.date = dateOnly;

                    string getProduct = $"select p.*, c.category_name from product p inner join category c on c.category_id = p.fk_category where p.product_id = {product_id}";
                    SqlCommand cmdProduct = new SqlCommand(getProduct, connection);
                    SqlDataAdapter adapterProduct = new SqlDataAdapter(cmdProduct);
                    DataTable dtProduct = new DataTable();
                    adapterProduct.Fill(dtProduct);

                    ProductModels pModels = new ProductModels();

                    pModels.product_id = Convert.ToInt32(dtProduct.Rows[0]["product_id"].ToString());
                    pModels.product_name = dtProduct.Rows[0]["product_name"].ToString() ?? string.Empty;
                    pModels.product_pic = dtProduct.Rows[0]["product_pic"].ToString() ?? string.Empty;
                    pModels.product_price = Convert.ToInt32(dtProduct.Rows[0]["product_price"].ToString());
                    pModels.product_description = dtProduct.Rows[0]["product_description"].ToString() ?? string.Empty;
                    pModels.fk_category = Convert.ToInt32(dtProduct.Rows[0]["fk_category"].ToString());
                    pModels.category = new CategoryModels() {
                        category_name = dtProduct.Rows[0]["category_name"].ToString()
                    };
                    cartModel.products = pModels;
                    cart.Add(cartModel);
                }

                connection.Close();

                return cart;
            }
        }

        [Authorize]
        [HttpPost]
        [Route("add_cart")]
        public IActionResult AddCart([FromBody] CartDTO request)
        {
            using (SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
            {
                string insertQuery = "insert into cart (fk_users,fk_product,date) values(@fk_users, @fk_product, @date)";

                try {
                    connection.Open();

                    SqlCommand command = new SqlCommand(insertQuery, connection);

                    command.Parameters.AddWithValue("@fk_users", request.fk_users);
                    command.Parameters.AddWithValue("@fk_product", request.fk_product);
                    command.Parameters.AddWithValue("@date", request.date);

                    command.ExecuteNonQuery();

                    return new JsonResult(new {message = "Add Cart Successfully"}) {StatusCode = 200};
                } catch(Exception ex) {
                    return new JsonResult(new {message = "Error: " + ex.Message}) {StatusCode = 500};
                }finally {
                    connection.Close();
                }
            }
        }

        [HttpDelete("delete/{id}")]
        public IActionResult DeleteItem(int id)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(_configuration.GetConnectionString("DefaultConnection")))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand("DELETE FROM cart WHERE cart_id = @Id", connection))
                    {
                        command.Parameters.AddWithValue("@Id", id);

                        int rowsAffected = command.ExecuteNonQuery();

                        if (rowsAffected > 0)
                        {
                            return new JsonResult(new {message = "Cart deleted successfully."}) {StatusCode = 200};
                        }
                        else
                        {
                            return new JsonResult(new {message = "Cart not found."}) {StatusCode = 404};
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                return new JsonResult(new {message = "Error: " + ex.Message}) {StatusCode = 500};
            }
        }
        
    }
}
