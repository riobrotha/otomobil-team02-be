using otomobil_team02_be.Models.DTO;

namespace otomobil_team02_be.Services.EmailService
{
  public interface IEmailService
  {
    void SendEmail(EmailDTO request);
  }

}

