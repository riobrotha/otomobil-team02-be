using MailKit.Net.Smtp;
using MailKit.Security;
using MimeKit;
using MimeKit.Text;
using otomobil_team02_be.Models.DTO;

namespace otomobil_team02_be.Services.EmailService
{
  public class EmailService : IEmailService
  {

    private readonly IConfiguration _config;

    public EmailService(IConfiguration config)
    {
      _config = config;
    }

    public void SendEmail(EmailDTO request)
    {
      var email = new MimeMessage();
      email.From.Add(MailboxAddress.Parse("riopambudhi51@gmail.com"));
      email.To.Add(MailboxAddress.Parse(request.To));
      email.Subject = request.Subject;
      email.Body = new TextPart(TextFormat.Html) {Text = request.Body};

      using var smtp = new SmtpClient();
      smtp.Connect(_config.GetSection("EmailHost").Value, Convert.ToInt32(_config.GetSection("EmailPort").Value),useSsl: true);
  		smtp.Authenticate(_config.GetSection("EmailUserName").Value, _config.GetSection("EmailPassword").Value);
  		smtp.Send(email);
  		smtp.Disconnect(true);
    }
  }
}

