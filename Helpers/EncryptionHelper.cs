using System.Text;
using System.Security.Cryptography;

namespace otomobil_team02_be.Helpers;
public class EncryptionHelper
{
  private static readonly byte[] _key = Encoding.UTF8.GetBytes("inikunci".PadRight(32, '\0')); // Ganti dengan kunci rahasia Anda sendiri

  public static string Encrypt(string plainText)
  {
      using var aesAlg = Aes.Create();
      aesAlg.Key = _key;
      aesAlg.GenerateIV();
      var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV);
      using var memoryStream = new MemoryStream();
      using var cryptoStream = new CryptoStream(memoryStream, encryptor, CryptoStreamMode.Write);
      using (var streamWriter = new StreamWriter(cryptoStream))
      {
          streamWriter.Write(plainText);
      }
      var encryptedData = memoryStream.ToArray();
      var combinedData = new byte[aesAlg.IV.Length + encryptedData.Length];
      Array.Copy(aesAlg.IV, 0, combinedData, 0, aesAlg.IV.Length);
      Array.Copy(encryptedData, 0, combinedData, aesAlg.IV.Length, encryptedData.Length);
      return Convert.ToBase64String(combinedData);
  }


  public static string Decrypt(string encryptedText)
  {
    var combinedData = Convert.FromBase64String(encryptedText);

    using var aesAlg = Aes.Create();
    aesAlg.Key = _key;

    var iv = new byte[aesAlg.IV.Length];
    Array.Copy(combinedData, 0, iv, 0, aesAlg.IV.Length);
    aesAlg.IV = iv;

    var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV);

    using var memoryStream = new MemoryStream();
    using (var cryptoStream = new CryptoStream(memoryStream, decryptor, CryptoStreamMode.Write))
    {
        cryptoStream.Write(combinedData, aesAlg.IV.Length, combinedData.Length - aesAlg.IV.Length);
    }

    var decryptedData = memoryStream.ToArray();
    return Encoding.UTF8.GetString(decryptedData);
  }
}
