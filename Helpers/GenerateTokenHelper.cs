using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;
using otomobil_team02_be.Models;

namespace otomobil_team02_be.Helpers;
public static class GenerateTokenHelper
{

  private static readonly IConfiguration _config;

  static GenerateTokenHelper()
  {
    var configBuilder = new ConfigurationBuilder().AddJsonFile("appsettings.json", optional: false, reloadOnChange: true).Build();
    _config = configBuilder;
  }
  public static string CreateToken(UserModel user)
  {
    var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_config["Jwt:Key"]));
    var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);

    var claims = new []
    {
      new Claim(ClaimTypes.Email, user.users_email ?? string.Empty),
      new Claim(ClaimTypes.Role, user.users_role ?? string.Empty)
    };

    var token = new JwtSecurityToken(_config["Jwt:Issuer"],
      _config["Jwt:Audience"],
      claims,
      expires: DateTime.Now.AddDays(1),
      signingCredentials: credentials
      );

    return new JwtSecurityTokenHandler().WriteToken(token);
  }
}
