﻿using otomobil_team02_be.Models;

namespace awang_otomobil_BE.Model.DTO
{
    public class MyClassModel
    {
        public int? myclass_id { get; set; }
        public int? fk_users { get; set; }
        public int? fk_product { get; set; }

        public DateTime date { get; set; }

        public ProductModels products { get; set; }
    }
}
