﻿namespace awang_otomobil_BE.Model.DTO
{
    public class InvoiceModel
    {
        public int? invoice_id { get; set; }
        public string? invoice_date { get; set; }
        public int? fk_payment { get; set; }
        public int? fk_users { get; set; }
    }
}
