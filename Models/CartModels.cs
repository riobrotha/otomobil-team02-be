﻿namespace otomobil_team02_be.Models
{
    public class CartModels
    {
        public int cart_id { get; set; }
        public int fk_users { get; set; }
        public int fk_product { get; set; }

        public DateTime date { get; set; }
        public ProductModels products { get; set; }
    }
}
