﻿namespace awang_otomobil_BE.Model.DTO
{
    public class DetailInvoiceModel
    {
        public int? detail_invoice_id { get; set; }
        public string? detail_invoice_name { get; set; }
        public int? detail_invoice_price { get; set; }
        public int? fk_invoice { get; set; }
        public int? fk_product { get; set; }
    }
}
