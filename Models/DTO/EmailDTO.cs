namespace otomobil_team02_be.Models.DTO;
public class EmailDTO
	{
		public string? To { get; set; }
		public string? Subject { get; set; }
		public string? Body { get; set; }
	}