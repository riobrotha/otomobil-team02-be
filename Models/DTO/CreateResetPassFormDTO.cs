using System.ComponentModel.DataAnnotations;

namespace otomobil_team02_be.Models.DTO;
public class CreateResetPassFormDTO
{
  [Required]
  public string? users_id { get; set; }
}
