namespace otomobil_team02_be.Models.DTO;
public class ActivateUserDTO
{
  public int? users_id { get; set; }

  public bool? users_is_active{ get; set; }
}
