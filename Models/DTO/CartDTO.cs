using System.ComponentModel.DataAnnotations;

namespace otomobil_team02_be.Models.DTO
{
    public class CartDTO
    {
        [Required] 
        public int fk_users { get; set; }

        [Required] 
        public int fk_product { get; set; }

        [Required] 
        public DateTime date { get; set; }
    }
}
