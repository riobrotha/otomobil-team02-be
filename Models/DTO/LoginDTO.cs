using System.ComponentModel.DataAnnotations;

namespace otomobil_team02_be.Models.DTO;
public class LoginDTO
{
  [Required]
  [EmailAddress]
  public string? email { get; set; }

  [Required]
  [MinLength(5)]
  public string? password { get; set;}
}
