using System.ComponentModel.DataAnnotations;

namespace otomobil_team02_be.Models.DTO;
public class EmailValidationDTO
{
  [Required]
  [EmailAddress]
  public string? email { get; set; }
}
