using System.ComponentModel.DataAnnotations;

namespace otomobil_team02_be.Models.DTO;
public class RegisterDTO
{
  [Required]
  public string? users_name { get; set; }
  [Required]
  [EmailAddress]
  public string? users_email { get; set;}

  [Required]
  [MinLength(5)]
  public string? users_password { get; set;}

  [MaxLength(100)]
  public string? users_address { get; set; }
}
