using System.ComponentModel.DataAnnotations;

namespace otomobil_team02_be.Models.DTO;
public class CreateNewPassDTO
{
  [Required]
  [MinLength(5)]
  public string? users_password { get; set; }

  public string? users_id { get; set; }
}
