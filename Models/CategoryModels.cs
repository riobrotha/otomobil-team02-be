﻿namespace otomobil_team02_be.Models
{
    public class CategoryModels
    {
        public int category_id { get; set; }

        public string category_name { get; set; }   

        public string category_description { get; set;}

        public string category_pic { get; set; }
    }
}
