﻿namespace otomobil_team02_be.Models
{
    public class ProductModels
    {
        public int product_id { get; set; }
        public string product_name { get; set;}
        public string product_pic { get; set;}
        public int product_price { get; set;}
        public string product_description { get; set;}
        public int fk_category { get; set;}
        
        public CategoryModels? category { get; set;}

    }
}
