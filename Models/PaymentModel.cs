﻿namespace awang_otomobil_BE.Model.DTO
{
    public class PaymentModel
    {
        public int? payment_id { get; set; }
        public string? payment_name { get; set; }
        public string? payment_number { get; set; }
        public string? payment_pic { get; set; }
        public bool? payment_is_active { get; set; }
    }
}
