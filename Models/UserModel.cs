using System.ComponentModel.DataAnnotations;

namespace otomobil_team02_be.Models;
public class UserModel
{
  public int? users_id { get; set; }
  public string? users_name { get; set; }
  public string? users_email { get; set;}
  public string? users_address { get; set; }
  public string? users_role{ get; set; }

  public bool? users_is_active { get; set;}
}
